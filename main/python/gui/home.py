# Code pour gerer l'authentification
# Author: Mouhammad Sylla @mouha09

from PySide2.QtWidgets import QMainWindow, QGraphicsDropShadowEffect, QMessageBox
from PySide2 import QtCore, QtGui, QtWidgets

from functools import partial
from services.user import User
from services.userServices import addUserToDatabase,getUsersFromDatabase

from ui_codes.home_window import Ui_HomeWindow


class HomeWindow(QMainWindow):
    def __init__(self, ctx):
        super().__init__()
        self.ctx = ctx
        self.ui = Ui_HomeWindow(ctx)
        self.ui.setupUi(self)

        self.setMaximumSize(QtCore.QSize(995, 732))
        self.setWindowTitle("Blog - News -- Administration")
        self.getUsers()
        self.btn_add_user = self.ui.btn_login
        self.btn_add_user.clicked.connect(partial(self.initiateUser))

    def initiateUser(self):
        
        self.prenom = self.ui.prenom.toPlainText()
        self.nom = self.ui.nom.toPlainText()
        self.username = self.ui.username.toPlainText()
        self.password = self.ui.password.toPlainText()
        
        user = User(self.prenom, self.nom, self.username, self.password)
        addUserToDatabase(user)
        self.addUserToListWidget(user)
        self.ui.prenom.clear()
        self.ui.nom.clear()
        self.ui.username.clear()
        self.ui.password.clear()
    
    def addUserToListWidget(self,user):
        userItem = QtWidgets.QListWidgetItem(f"{user.prenom} {user.nom}")
        userItem.setTextAlignment(QtCore.Qt.AlignCenter)
        font = QtGui.QFont()
        font.setPointSize(15)
        userItem.setFont(font)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.BrushStyle.SolidPattern)
        userItem.setBackground(brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 11, 22))
        brush.setStyle(QtCore.Qt.BrushStyle.NoBrush)
        userItem.setForeground(brush)
        userItem.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsEditable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsDropEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        userItem.user = user
        self.ui.userList.addItem(userItem)
    
    def getUsers(self):
        users = getUsersFromDatabase()
        for user in users:
            user = User(user[0], user[1] )
            self.addUserToListWidget(user)
            
    
    # Fonction permettant d'autentifier l'utilisateur à partir des identpifiants renseignés dans la base de données 
    # def authentication(self,ctx):
    #     self.username = self.ui.te_username.toPlainText()
    #     self.mdp = self.ui.te_mdp.toPlainText()
        
    #     userInformations = checkUser(self.username, self.mdp)

    #     if(userInformations != False):
    #         print(userInformations)
    #         # self.username = f"{userInformations[0]} {userInformations[1]}"
            
    #         # self.home_window = HomeWindow(ctx,self.username)
    #         # self.home_window.show()
    #         # self.close()
    #     else:
    #         self.displayError()
    #         authentication(self,ctx)
        
    def displayError(self):
        message = QMessageBox(self)
        message.setStyleSheet("background-color: white")
        message.setWindowTitle("Erreur !")
        message.setIcon(QMessageBox.Critical)
        message.setText('Login ou mot de passe incorrect. Veuillez reessayer')
        message.setStandardButtons(QMessageBox.Ok)
        message.exec_()
       
