# Code pour gerer l'authentification 
# Author: Mouhammad Sylla @mouha09

from PySide2.QtWidgets import QMainWindow, QGraphicsDropShadowEffect, QMessageBox
from PySide2 import QtCore, QtGui, QtWidgets

from functools import partial

from  services.userServices import checkUser
from  gui.home import HomeWindow

from ui_codes.login_window import Ui_LoginWindow

class LoginWindow(QMainWindow):
    def __init__(self,ctx):
        super().__init__()
        self.ctx = ctx
        self.ui = Ui_LoginWindow(ctx)
        self.ui.setupUi(self)
        
        self.setMaximumSize(QtCore.QSize(995, 732))
        
        
        self.setWindowTitle("Blog - News -- Administration")
        self.btn_login = self.ui.btn_login
        self.btn_login.clicked.connect(partial(self.authentication,self.ctx))
    
    #Fonction permettant d'autentifier l'utilisateur à partir des identifiants renseignés dans la base de données 
    def authentication(self,ctx):
        self.username = self.ui.te_username.toPlainText()
        self.mdp = self.ui.te_mdp.toPlainText()
        
        userInformations = checkUser(self.username, self.mdp)

        if(userInformations != False):
            if(userInformations[2] == "ADMIN"):
                self.home_window = HomeWindow(ctx)
                self.home_window.show()
                self.close()
            else:
                self.displayNotAuthorized()
        else:
            self.displayError()
            authentication(self,ctx)
        
    def displayError(self):
        message = QMessageBox(self)
        message.setStyleSheet("background-color: white")
        message.setWindowTitle("Erreur !")
        message.setIcon(QMessageBox.Critical)
        message.setText('Login ou mot de passe incorrect. Veuillez reessayer')
        message.setStandardButtons(QMessageBox.Ok)
        message.exec_()
       
    def displayNotAuthorized(self):
        message = QMessageBox(self)
        message.setStyleSheet("background-color: white")
        message.setWindowTitle("Erreur !")
        message.setIcon(QMessageBox.Critical)
        message.setText('Vous n\'avez pas les droits d\'accés necessaires')
        message.setStandardButtons(QMessageBox.Ok)
        message.exec_()
