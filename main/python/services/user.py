from services.userServices import addUserToDatabase


class User():
    def __init__(self, prenom="", nom="", username="", password="") -> None:
        self.prenom = prenom
        self.nom = nom
        self.username = username
        self.password = password
        super().__init__()
    
    def addUser(self, user):
        addUserToDatabase(user)