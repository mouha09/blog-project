from config.database import databaseConnexion
from mysql.connector import Error
import hashlib


try:
    databaseInstance = databaseConnexion()
except Error as e:
    exit()

def checkUser(username, password):
    """Fonction permettant d'authentifier un utilisateur en fonction des identifiants
       Author : Mouhammad Sylla @mouha09

    Args:
        username ([string]): [nom d'utilisateur]
        password ([string]): [mot de passe]

    Returns:
        [tuple]: [retourne le nom et le prenom]
    """
 
    
    request = "select prenom,nom,role from users where username=%s and mdp=%s"
    params = (username, hashlib.md5(password.encode("utf-8")).hexdigest()) 
    cursor = databaseInstance.cursor()
    cursor.execute(request,params)
    
    result = cursor.fetchone()
    cursor.close()

    if result : 
        return result
    else:
        return False
    
    


def addUserToDatabase(user):
 
    request = "insert into users (prenom,nom,username,mdp,role) values (%s,%s,%s,%s,%s)"
    password = user.password
    params = (user.prenom, user.nom, user.username,  hashlib.md5(password.encode("utf-8")).hexdigest(), 'EDITOR') 
    cursor = databaseInstance.cursor() 
    cursor.execute(request,params)
    
    databaseInstance.commit()
    cursor.close()
    

def getUsersFromDatabase():
    
    request = "select prenom,nom from users"
    cursor = databaseInstance.cursor()
    cursor.execute(request)
    users = cursor.fetchall()
    cursor.close()
    
    return users
