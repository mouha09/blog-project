
from PySide2.QtCore import (QCoreApplication, QMetaObject, QObject, QPoint,
    QRect, QSize, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QLinearGradient, QPalette, QPainter, QPixmap,
    QRadialGradient)
from PySide2 import QtWidgets,QtCore,QtGui


class Ui_HomeWindow(object):
    def __init__(self,ctx):
        super().__init__()
        self.ctx = ctx
            
    def setupUi(self, HomeWindow):
        HomeWindow.setObjectName("HomeWindow")
        HomeWindow.resize(995, 732)
        HomeWindow.setStyleSheet("background-color:#000F1B;")
        self.centralwidget = QtWidgets.QWidget(HomeWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.userList = QtWidgets.QListWidget(self.centralwidget)
        self.userList.setGeometry(QtCore.QRect(0, 20, 521, 661))
        self.userList.setStyleSheet("color:white;padding-top:20;padding-left:40;padding-right:40")
        self.userList.setObjectName("userList")
        self.userList.setSpacing(10)
        item = QtWidgets.QListWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        font = QtGui.QFont()
        font.setPointSize(15)
        item.setFont(font)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.BrushStyle.SolidPattern)
        item.setBackground(brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 11, 22))
        brush.setStyle(QtCore.Qt.BrushStyle.NoBrush)
        item.setForeground(brush)
        item.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsEditable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsDropEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
 
        item = QtWidgets.QListWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        font = QtGui.QFont()
        font.setPointSize(15)
        item.setFont(font)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.BrushStyle.SolidPattern)
        item.setBackground(brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 11, 22))
        brush.setStyle(QtCore.Qt.BrushStyle.SolidPattern)
        item.setForeground(brush)
        item.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsEditable|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsDropEnabled|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
     
        self.authentication_box = QtWidgets.QFrame(self.centralwidget)
        self.authentication_box.setGeometry(QtCore.QRect(520, 20, 461, 661))
        self.authentication_box.setStyleSheet("background-color:rgb(0, 11, 22); ")
        self.authentication_box.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.authentication_box.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.authentication_box.setObjectName("authentication_box")
        self.prenom = QtWidgets.QTextEdit(self.authentication_box)
        self.prenom.setGeometry(QtCore.QRect(80, 140, 321, 41))
        self.prenom.setStyleSheet("border:1px solid white;\n"
"text-align:center;\n"
"padding-top:7px;\n"
"padding-left:8px;\n"
"color:rgb(255, 0, 52);border-radius:20")
        self.prenom.setObjectName("prenom")
        self.btn_login = QtWidgets.QPushButton(self.authentication_box)
        self.btn_login.setGeometry(QtCore.QRect(150, 520, 181, 51))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.btn_login.setFont(font)
        self.btn_login.setCursor(QtGui.QCursor(QtCore.Qt.CursorShape.OpenHandCursor))
        self.btn_login.setStyleSheet("QPushButton{\n"
"    /*background-color:#FF0000;*/\n"
"    background-color:#EF3F3F;\n"
"    color: white;\n"
"    border-radius: 20;\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"background-color:white;\n"
"color:#EF3F3F;\n"
"}")
        self.btn_login.setObjectName("btn_login")
        self.nom = QtWidgets.QTextEdit(self.authentication_box)
        self.nom.setGeometry(QtCore.QRect(80, 230, 321, 41))
        self.nom.setStyleSheet("border:1px solid white;\n"
"text-align:center;\n"
"padding-top:7px;\n"
"padding-left:8px;color:rgb(255, 0, 52);border-radius:20;")
        self.nom.setObjectName("nom")
        self.username = QtWidgets.QTextEdit(self.authentication_box)
        self.username.setGeometry(QtCore.QRect(80, 320, 321, 41))
        self.username.setStyleSheet("border:1px solid white;\n"
"text-align:center;\n"
"padding-top:7px;\n"
"padding-left:8px;color:rgb(255, 0, 52); border-radius:20;")
        self.username.setObjectName("username")
        self.password = QtWidgets.QTextEdit(self.authentication_box)
        self.password.setGeometry(QtCore.QRect(80, 410, 321, 41))
        self.password.setStyleSheet("border-radius:20;border:1px solid white;\n"
"text-align:center;\n"
"padding-top:7px;\n"
"padding-left:8px;color:rgb(255, 0, 52);")
        self.password.setObjectName("password")
        self.add_user_title = QtWidgets.QFrame(self.authentication_box)
        self.add_user_title.setGeometry(QtCore.QRect(130, 30, 211, 51))
        self.add_user_title.setStyleSheet("background-color:white;border-radius:none;")
        self.add_user_title.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.add_user_title.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.add_user_title.setObjectName("add_user_title")
        self.label = QtWidgets.QLabel(self.add_user_title)
        self.label.setGeometry(QtCore.QRect(20, 10, 161, 31))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setStyleSheet("color : rgb(0, 11, 22);")
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        HomeWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(HomeWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 995, 22))
        self.menubar.setObjectName("menubar")
        HomeWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(HomeWindow)
        self.statusbar.setObjectName("statusbar")
        HomeWindow.setStatusBar(self.statusbar)

        self.retranslateUi(HomeWindow)
        QtCore.QMetaObject.connectSlotsByName(HomeWindow)

    def retranslateUi(self, HomeWindow):
        _translate = QtCore.QCoreApplication.translate
        HomeWindow.setWindowTitle(_translate("HomeWindow", "MainWindow"))
        self.userList.setSortingEnabled(False)
        __sortingEnabled = self.userList.isSortingEnabled()
        self.userList.setSortingEnabled(False)
        
        self.userList.setSortingEnabled(__sortingEnabled)
        self.prenom.setHtml(_translate("HomeWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'.AppleSystemUIFont\'; font-size:13pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))
        self.prenom.setPlaceholderText(_translate("HomeWindow", "Prenom"))
        self.btn_login.setText(_translate("HomeWindow", "Ajouter "))
        self.nom.setHtml(_translate("HomeWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'.AppleSystemUIFont\'; font-size:13pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))
        self.nom.setPlaceholderText(_translate("HomeWindow", "Nom"))
        self.username.setHtml(_translate("HomeWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'.AppleSystemUIFont\'; font-size:13pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))
        self.username.setPlaceholderText(_translate("HomeWindow", "Nom d\'utilsateur"))
        self.password.setHtml(_translate("HomeWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'.AppleSystemUIFont\'; font-size:13pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))
        self.password.setPlaceholderText(_translate("HomeWindow", "Mot de passe"))
        self.label.setText(_translate("HomeWindow", "Ajout Utilisateur"))
