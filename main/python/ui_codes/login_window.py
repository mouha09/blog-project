

from PySide2.QtCore import (QCoreApplication, QMetaObject, QObject, QPoint,
    QRect, QSize, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QLinearGradient, QPalette, QPainter, QPixmap,
    QRadialGradient)
from PySide2 import QtWidgets,QtCore,QtGui


class Ui_LoginWindow(object):
    def __init__(self,ctx):
        super().__init__()
        self.ctx = ctx
        
    def setupUi(self, LoginWindow):
        LoginWindow.setObjectName("LoginWindow")
        LoginWindow.resize(995, 732)
        LoginWindow.setAutoFillBackground(False)
        LoginWindow.setStyleSheet("background-color:#000F1B;")
        self.centralwidget = QtWidgets.QWidget(LoginWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.authentication_box = QtWidgets.QFrame(self.centralwidget)
        self.authentication_box.setGeometry(QtCore.QRect(310, 130, 441, 401))
        self.authentication_box.setStyleSheet("background-color:white; border-radius:20%;")
        self.authentication_box.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.authentication_box.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.authentication_box.setObjectName("authentication_box")
        self.te_username = QtWidgets.QTextEdit(self.authentication_box)
        self.te_username.setPlaceholderText('Nom d\'utilisateur')
        self.te_username.setGeometry(QtCore.QRect(80, 140, 321, 41))
        self.te_username.setStyleSheet("border:1px solid black;\n"
"text-align:center;\n"
"padding-top:7px;\n"
"padding-left:8px;")
        self.te_username.setObjectName("te_username")
        self.btn_login = QtWidgets.QPushButton(self.authentication_box)
        self.btn_login.setGeometry(QtCore.QRect(130, 320, 181, 51))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.btn_login.setFont(font)
        self.btn_login.setCursor(QtGui.QCursor(QtCore.Qt.CursorShape.OpenHandCursor))
        self.btn_login.setStyleSheet("QPushButton{\n"
"    /*background-color:#FF0000;*/\n"
"    background-color:#EF3F3F;\n"
"    color: white;\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"background-color:#000F1B;\n"
"}")
        self.btn_login.setObjectName("btn_login")
        self.label = QtWidgets.QLabel(self.authentication_box)
        self.label.setGeometry(QtCore.QRect(80, 40, 271, 31))
        font = QtGui.QFont()
        font.setPointSize(25)
        self.label.setFont(font)
        self.label.setStyleSheet("")
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.te_mdp = QtWidgets.QTextEdit(self.authentication_box)
        self.te_mdp.setPlaceholderText('Mot de passe')
        self.te_mdp.setGeometry(QtCore.QRect(80, 240, 321, 41))
        self.te_mdp.setStyleSheet("border:1px solid black;\n"
"text-align:center;\n"
"padding-top:7px;\n"
"padding-left:8px;")
        self.te_mdp.setObjectName("te_mdp")
        LoginWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(LoginWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 995, 22))
        self.menubar.setObjectName("menubar")
        LoginWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(LoginWindow)
        self.statusbar.setObjectName("statusbar")
        LoginWindow.setStatusBar(self.statusbar)

        self.retranslateUi(LoginWindow)
        QtCore.QMetaObject.connectSlotsByName(LoginWindow)

    def retranslateUi(self, LoginWindow):
        _translate = QtCore.QCoreApplication.translate
        LoginWindow.setWindowTitle(_translate("LoginWindow", "MainWindow"))
        self.te_username.setHtml(_translate("LoginWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'.AppleSystemUIFont\'; font-size:13pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" color:#97989a;\"></span></p></body></html>"))
        self.btn_login.setText(_translate("LoginWindow", "Se Connecter"))
        self.label.setText(_translate("LoginWindow", "Authentification"))
        self.te_mdp.setHtml(_translate("LoginWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'.AppleSystemUIFont\'; font-size:13pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" color:#97989a;\"></span></p></body></html>"))
