package com.example.blog;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.jaxws.SimpleJaxWsServiceExporter;



//@Configuration
public class MyConfig {


   // @Bean
    public  SimpleJaxWsServiceExporter getJWS (){
        SimpleJaxWsServiceExporter serviceExporter= new SimpleJaxWsServiceExporter();
        serviceExporter.setBaseAddress("http://localhost:8080/maBanque");
        return serviceExporter;
    }
}
