package com.example.blog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.example.blog.entities.Category;

@CrossOrigin("*")
@Repository
public interface CategoryRepository extends JpaRepository<Category,Long> {

}
