package com.example.blog.repositories;


import com.example.blog.entities.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;


@CrossOrigin("*")
@Repository
public interface ArticleRepository extends JpaRepository<Article,Long> {

}