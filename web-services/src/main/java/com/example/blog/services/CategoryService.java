package com.example.blog.services;


import com.example.blog.entities.Category;
import com.example.blog.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;


    @RequestMapping(value="/categories",method=RequestMethod.GET)
    public List<Category> getCategories(){
        return categoryRepository.findAll();
    }

    @RequestMapping(value="/categories/{id}",method=RequestMethod.GET)
    public Optional<Category> getCategory(@PathVariable Long id){
        return categoryRepository.findById(id);
    }

    @RequestMapping(value="/categories/{id}",method=RequestMethod.DELETE)
    public boolean supprimer(@PathVariable Long id){
        categoryRepository.deleteById(id);
        return true;
    }

    @RequestMapping(value="/categories/{id}",method=RequestMethod.PUT)
    public Category update(@PathVariable Long id,@RequestBody Category c){
        c.setId(id);
        return categoryRepository.save(c);
    }


    @RequestMapping(value="/categories",method=RequestMethod.POST)
    public Category save(@RequestBody Category c){
        return categoryRepository.save(c);
    }
}
