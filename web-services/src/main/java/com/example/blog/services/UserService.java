package com.example.blog.services;

import com.example.blog.entities.User;
import com.example.blog.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController

public class UserService {

    @Autowired
    private UserRepository userRepository;


    @RequestMapping(value="/users",method= RequestMethod.GET)
    public List<User> getUsers(){
        return userRepository.findAll();
    }



    @RequestMapping(value="/users/{id}",method=RequestMethod.GET)
    public Optional<User> getUser(@PathVariable Long id){
        return userRepository.findById(id);
    }


    @RequestMapping(value="/users/{id}",method=RequestMethod.DELETE)
    public boolean supprimer(@PathVariable Long id){
        userRepository.deleteById(id);
        return true;
    }

    @RequestMapping(value="/users/{id}",method=RequestMethod.PUT)
    public User update(@PathVariable Long id,@RequestBody User c){
        c.setId(id);
        return userRepository.save(c);
    }


    @RequestMapping(value="/users",method=RequestMethod.POST)
    public User save(@RequestBody User c){
        return userRepository.save(c);
    }
}
