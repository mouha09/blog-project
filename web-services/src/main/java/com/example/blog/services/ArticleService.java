package com.example.blog.services;


import com.example.blog.entities.Article;
import com.example.blog.entities.Category;
import com.example.blog.repositories.ArticleRepository;
import com.example.blog.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ArticleService {


    @Autowired
    private ArticleRepository articleRepository;


    @RequestMapping(value="/articles",method= RequestMethod.GET)
    public List<Article> getArticles(){
        return articleRepository.findAll();
    }

    @RequestMapping(value="/articles/{id}",method=RequestMethod.GET)
    public Optional<Article> getArticle(@PathVariable Long id){
        return articleRepository.findById(id);
    }

    @RequestMapping(value="/articles/{id}",method=RequestMethod.DELETE)
    public boolean supprimer(@PathVariable Long id){
        articleRepository.deleteById(id);
        return true;
    }

    @RequestMapping(value="/articles/{id}",method=RequestMethod.PUT)
    public Article update(@PathVariable Long id, @RequestBody Article c){
        c.setId(id);
        return articleRepository.save(c);
    }



    @RequestMapping(value="/articles",method=RequestMethod.POST)
    public  Article save(@RequestBody  Article c){
        return articleRepository.save(c);
    }
}
