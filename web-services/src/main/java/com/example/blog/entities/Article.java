package com.example.blog.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


@Entity
@Data @NoArgsConstructor @AllArgsConstructor
public class Article implements Serializable {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;
    private String titre;
    private String contenu;
    @Temporal(TemporalType.DATE)
    private Date datecreation;
    @Temporal(TemporalType.DATE)
    private Date datamodification;
    @ManyToOne
    private Category category;

}

